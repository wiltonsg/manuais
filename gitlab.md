# Tutorial de instalação e configuração do GitLab no Ubuntu

## Instalando o Git e o GitGUI no Ubuntu

1. Para instalar o Git abra o Terminal do Ubuntu e digite os comandos abaixo:

```
sudo apt-get update
sudo apt-get install git
```

2. Para instalar o GitGUI abra o Terminal do Ubuntu e digite os comandos abaixo:
*Nota: O gitk instala o GitGUI que é a interface gráfica do Git.*

```
sudo apt-get update
sudo apt-get install gitk
```
## Criando um repositório e comitando

1. No Terminal digite os comandos abaixo e informe ao Git os seus dados, eles irão identificar seus commits.
*Nota: Os comandos são executados apenas na primeira vez que for configurar o Git, coloque o ‘NomeSobrenome’ sem espaços. O email que está informando é interessante que seja o mesmo da conta do GitLab, para ele identificar todos seus commits, mais informações de como criar conta GitLab ver passo ‘Configurando e compartilhamento no GitLab’.*

```
git config --global user.name "NomeSobrenome"
git config --global user.email "seu_email@email.com"
```

2. Clique com o direito na pasta onde deseja criar o repositório local e selecione o Terminal. Para inicializar um repositório Git nesta pasta digite o comando abaixo:

```
git init
```

3. Agora para testar o commit digite os comandos abaixo:
Nota: Primeiro comando cria um arquivo “teste.txt” vazio no repositório que foi inicializado. O segundo adiciona todos as alterações do repositório ao índice. E o terceiro comita todos os arquivos que estão no índice e que foram modificados.

```
touch teste.txt
git add .
git commit -m "Primeiro commit"
```

## Configurando e compartilhamento no GitLab

*Nota: Todos as etapas realizadas até este ponto ocorreram no repositório local. Esse processo é necessário apenas a primeira vez que for comunicar seu computador com o GitLab. Para comitar seu repositório local no GitLab primeiramente você precisa de uma conta e de uma chave SSH(Secure Shell – protocolo de rede criptográfico).*

1. Entre no site do GitLab na aba ‘Register’ e crie uma conta.
*Nota: Esse passo só é necessário caso não tenha conta no GitLab.*

2. Logue em sua conta GitLab.

3. Crie a chave SSH através do Terminal, digite o comando abaixo:
*Nota: Informe o email que está cadastrado no GitLab. Na primeira pergunta aperte ‘Enter’ para deixar padrão o nome da chave que está criando. Informe uma senha e repita a senha informada.*

```
ssh-keygen -t rsa -C "seu_email@email.com"
```

4. Para abrir a chave SSH criada, digite o comando abaixo:
*Nota: Se você alterou o nome padrão da chave no lugar de ‘id_rsa’ coloque o nome que você informou.*

```
gedit ~/.ssh/id_rsa.pub
```

5. Na sua conta do GitLab entre em ‘Settings’ e procure a aba ‘SSH Keys’.

6. Copie todo o conteúdo da chave SSH que você abriu no bloco de notas e cole no campo ‘Key’, informe um titulo para identificar o computador onde a chave foi gerada.

7. Para verificar se está funcionando corretamente digite o comando abaixo no Terminal:
*Nota: Quando der o comando ele irá solicitar uma senha, ela é a mesma que você cadastrou quando criou sua chave SSH.*

```
ssh -T git@gitlab.com
```

8. Se a mensagem no Terminal for ‘Welcome to GitLab, Nome Sobrenome(nome e sobrenome cadastrados no site do GitLab)’ significa que está tudo funcionando, caso seja um erro, por favor repita os passos do item 3.

## Criando o repositório remoto

1. Em sua conta GitLab clique em ‘New Project’.

2. Adicione um nome ao projeto e escolha qual o nível de visibilidade que deseja que o projeto tenha.
*Nota: No nome do projeto não é permitido espaço, substitua por ‘-‘, ‘_’ ou ‘.’, não é possível iniciar com ‘-‘ e nem terminar com ‘.git’ ou ‘.atom’. Existe três níveis de visibilidade: Primeiro é o ‘Private’(o acesso ao projeto tem que ser concedido explicitamente a cada usuário); Segundo é o ‘Internal’(o acesso ao projeto é para qualquer usuário logado no GitLab); Terceiro é o ‘Public’(o acesso é a qualquer pessoa logada ou não no GitLab).*

3. Na janela do Terminal digite o comando abaixo para atribuir um nome a URL do seu projeto no GitLab:
*Nota: O nome curto é o nome para referenciar o projeto remoto, assim não é necessário digitar toda a URL para dar um commit no remoto. A URL você pode copiar de dentro do projeto que você criou, existem um campo com ela.*

```
git remote add nome_curto
git@gitlab.com:seu_login/nome_do_repositorio.git
```

4. Para atualizar seu projeto no GitLab com as informações do projeto local digite o comando abaixo:

```
git push nome_curto master
```

Fonte: [Big Data no Agronegócio](https://bigdataagronegocio.wordpress.com/2017/03/12/tutorial-de-instalacao-e-configuracao-do-gitlab-no-ubuntu/)
